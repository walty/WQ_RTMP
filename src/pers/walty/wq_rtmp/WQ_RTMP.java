package pers.walty.wq_rtmp;

import java.io.IOException;
import java.util.List;
import java.util.Scanner;

import pers.walty.wq_rtmp.server.RTMPServerSocket;
import pers.walty.wq_rtmp.server.RTMPSocket;


public class WQ_RTMP {
	
	private static RTMPServerSocket rtmpServerSocket;
	static Scanner scanner = new Scanner(System.in);
	
	
	public static void main(String[] args) {
		boolean flag = true;
		System.out.println("Start Server��");
		try {
			rtmpServerSocket = new RTMPServerSocket();
			rtmpServerSocket.start();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			System.out.println("The RTMPServerSocket's port be used!");
		}
		System.out.println("RTMPServerSocket is started!");
		
		//
		//System.out.println("HTTPServerSocket is started!");
		
		
		helper();
		while(flag){
			System.out.println("Please select:");
			choice(scanner.nextInt());
		}
	}
	
	private static void choice(int choice){
		switch(choice){
		case 0:
			helper();
			break;
		case 1:
			getRtmpClientNum();
			break;
		case 2:
			rtmpServerList();
			break;
		case 3:
			checkRtmpSocketLog();
		default:
			System.out.println("dont have this choice!");
		}
	}
	
	private static void checkRtmpSocketLog() {
		// TODO Auto-generated method stub
		boolean flag = false;
		int len = rtmpServerSocket.getLinkCount();
		do{
			System.out.println("Please input rtmp client id");
			int id = scanner.nextInt();
			if(id >= 0 && id < len){
				System.out.println(rtmpServerSocket.getLinkList().get(id).getLog());
			}else{
				flag = true;
			}
		}while(flag);
	}

	private static void getRtmpClientNum() {
		// TODO Auto-generated method stub
		System.out.println("Rtmp Client Count : " + rtmpServerSocket.getLinkCount());
	}

	private static void rtmpServerList() {
		// TODO Auto-generated method stub
		int len = rtmpServerSocket.getLinkCount();
		List<RTMPSocket> list = rtmpServerSocket.getLinkList();
		for(int i = 0 ; i < len ; i++){
			System.out.println( i + " " + list.get(i).getName());
		}
	}

	private static void helper(){
		System.out.println("You can choice:");
		System.out.println("0 show helper");
		System.out.println("1 show rtmp Client Count");
		System.out.println("2 show rtmp ClientList");
		System.out.println("3 show rtmp Client log");
	}
	
}
