package pers.walty.wq_rtmp.server;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.Socket;
import pers.walty.wq_rtmp.server.message.MessageFactory;
import pers.walty.wq_rtmp.server.message.chunk.BasicChunk;
import pers.walty.wq_rtmp.server.message.chunk.ChunkBuilder;
import pers.walty.wq_rtmp.server.message.chunk.SendChunk;
import pers.walty.wq_rtmp.server.message.chunk.ShakeChunk;
import pers.walty.wq_rtmp.server.util.amf.ObjAmf;

public class RTMPSocket extends Thread{
	private final String TAG = this.getClass().getSimpleName();
	private Socket socket;
	private int chunkSize = ChunkBuilder.SHAKE_CHUNK_LENGTH + 1;
	private boolean isRun = true;
	private StringBuilder log; 
	private String socketName = "test";
	private BufferedInputStream bis;
	private BufferedOutputStream bos;
	private int dataSize = 0;
	private int basicSize = 0;
	private int msgHeaderSize = 0;
	private MessageFactory msgFactory;
	
	public RTMPSocket(Socket socket){
		super();
		log = new StringBuilder();
		this.socket = socket;
		this.msgFactory = new MessageFactory(this);
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		super.run();
		try {
			bis = new BufferedInputStream(socket.getInputStream());
			bos = new BufferedOutputStream(socket.getOutputStream());
			ShakeChunk c1 = shake1();
			ShakeChunk s1 = c1.getNextChunk();
			shake2(s1,c1);
			if(isRun){setLog(TAG,"shake is successful!!!");}
			while(isRun){
				//int len = bis.available();
				msgFactory.builder(bis);
				//SendChunk sc = new SendChunk(ChunkBuilder.makeAckChunk(len));
				//pushBytes(sc.getContent());
			}
			bis.close();
			bos.close();
			socket.close();
			setLog(TAG,"the rtmp over!");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			setLog(TAG,"Client is unline!");
			setLog(TAG,e.getLocalizedMessage());
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public int getDataSize(){
		return this.dataSize;
	}
	
	public RTMPSocket setDataSize(int dataSize){
		this.dataSize = dataSize;
		makeChunkSize();
		return this;
	}
	
	
	
	private ShakeChunk shake1() throws IOException{
		byte[] b = getByteArray();
		ShakeChunk c0 = ChunkBuilder.firstShake(b);
		ShakeChunk c1 = c0.getNextChunk();
		ShakeChunk s0 = ChunkBuilder.reFirstShake(c0);
		ShakeChunk s1;
		if(null == s0){
			this.isRun = false;
			s1 = null;
		}else{
			bos.write(s0.getContent());
			bos.flush();
			s1 = s0.getNextChunk();
			bos.write(s1.getContent());
			bos.flush();
			c1.setNextChunk(s1);
			chunkSize--;
		}
		return c1;
	}
	
	private void shake2(ShakeChunk s1,ShakeChunk c1) throws IOException{
		if(null == s1){
			isRun = false;
			this.setLog(TAG, "shake is failed");
		}else{
			byte[] b = getByteArray();
			ShakeChunk c2 = ChunkBuilder.secondShake(b);
			ShakeChunk s2 = ChunkBuilder.reSecondShake(s1, c2, c1);
			if(null == s2){
				isRun = false;
				this.setLog(TAG, "shake is failed");
			}else{
				bos.write(s2.getContent());
				bos.flush();
			}
		}
	}
	
	public void close(){
		isRun = false;
	}
	
	private byte[] getByteArray() throws IOException{
		byte[] b = new byte[chunkSize];
		bis.read(b);
		return b;
	}
	
	private void makeChunkSize(){
		chunkSize = dataSize + basicSize + msgHeaderSize;
	}
	
	public void setSocketName(String name){
		this.socketName = name;
	}
	
	public String getSocketName(){
		return this.socketName;
	}
	
	public void setLog(String tag,String content){
		log.append("["+tag+"]:"+content+"\n");
	}
	
	public String getLog(){
		return log.toString();
	}
	
	public void pushBytes(byte[] bs) throws IOException{
		bos.write(bs);
		bos.flush();
	}
	
	public void pushBytes(BasicChunk bc) throws IOException{
		pushBytes(new SendChunk(bc).getContent());
	}
	
	public void pushBytes(ObjAmf objAmf) throws IOException{
		pushBytes(ChunkBuilder.amfObj2BC(objAmf));
	}
}
