package pers.walty.wq_rtmp.server.message;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import pers.walty.wq_rtmp.server.RTMPSocket;
import pers.walty.wq_rtmp.server.message.chunk.ChunkFactory;

public class MessageFactory {
	private Map<byte[],MsgBean> saveChunk;
	private ChunkFactory cf;
	private RTMPSocket rtmpSocket;
	private MsgWorker msgWorker;
	public MessageFactory(RTMPSocket rtmpSocket){
		cf = new ChunkFactory(this);
		saveChunk = new HashMap<byte[],MsgBean>();
		this.rtmpSocket = rtmpSocket;
		this.msgWorker = new MsgWorker(this);
	}
	
	public void close(){
		this.rtmpSocket.close();
		this.msgWorker.close();
	}
	
	public Map<byte[], MsgBean> getSaveChunk() {
		return saveChunk;
	}
	
	public ChunkFactory getCf() {
		return cf;
	}
	
	public void setSaveChunk(Map<byte[], MsgBean> saveChunk) {
		this.saveChunk = saveChunk;
	}
	
	public RTMPSocket getRtmpSocket() {
		return rtmpSocket;
	}
	
	public void builder(BufferedInputStream bis) throws IOException, InterruptedException{
		if(null == saveChunk){
			saveChunk = new HashMap<byte[],MsgBean>();
		}
		cf.builder(bis,this);
	}
	
	public void checkFull() throws IOException{
		Iterator<Entry<byte[], MsgBean>>  iterator = saveChunk.entrySet().iterator();
		while(iterator.hasNext()){
			Entry<byte[],MsgBean> e = iterator.next();
			MsgBean mb = e.getValue();
			if(mb.isFull()){
				iterator.remove();
				//saveChunk.remove(e.getKey());
				dealMSG(mb.makeMsg());
			}
		}
	}
	
	public void dealMSG(Message msg){
		
		msgWorker.working(msg);
	}
	
	
}
