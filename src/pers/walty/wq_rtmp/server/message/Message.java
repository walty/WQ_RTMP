package pers.walty.wq_rtmp.server.message;

import pers.walty.wq_rtmp.server.util.ByteUtil;

public class Message {
	private byte[] content;
	private byte[] msgStream;
	private int msgTime;
	private byte msgType;
	private int msgLen;
	
	public Message(byte[] msgStream,byte msgType,byte[] msgLen,byte[] msgTime,byte[] content){
		this.msgStream = msgStream;
		this.msgType = msgType;
		this.msgLen = ByteUtil.bytesToInt(msgLen);
		this.msgTime = ByteUtil.bytesToInt(msgTime);
		this.content = content;
	}
	
	public int getMsgTime(){
		return msgTime;
	}
	
	public byte getMsgType() {
		return msgType;
	}
	
	public byte[] getMsgStream() {
		return msgStream;
	}
	
	public int getMsgLen() {
		return msgLen;
	}
	
	public byte[] getContent(){
		return content;
	}
	
	public Message setContent(byte[] content){
		this.content = content;
		return this;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "[msgTime = "+this.getMsgTime()
				+"\n,msgType = "+this.getMsgType()
				+"\n,msgSteam = "+ByteUtil.bytesToInt(this.getMsgStream())
				+"\n,msgLen = "+this.getMsgLen()
				+"\n,msgContent= "+ByteUtil.bytes2Str(this.getContent())+"]";
	}
}
