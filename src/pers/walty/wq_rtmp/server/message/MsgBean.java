package pers.walty.wq_rtmp.server.message;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;

import pers.walty.wq_rtmp.server.message.chunk.BasicChunk;
import pers.walty.wq_rtmp.server.util.ByteUtil;

public class MsgBean {
	int nowLen;
	int msgLen;
	LinkedList<BasicChunk> list;
	public MsgBean(BasicChunk bc){
		list = new LinkedList<BasicChunk>();
		msgLen = ByteUtil.bytesToInt(bc.getMsgHeader_length());
		nowLen = 0;
		this.add(bc);
	}
	
	public int getNowLen(){
		return nowLen;
	}
	
	public boolean isFull(){
		return msgLen == nowLen;
	}
	
	public void add(BasicChunk bc){
		this.nowLen += bc.getData().length;
		list.add(bc);
	}
	
	public Message makeMsg() throws IOException{
		Iterator<BasicChunk> i1 = list.iterator();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		while(i1.hasNext()){
			baos.write(i1.next().getData());
		}
		byte[] msgStream = list.getFirst().getMsgHeader_stream();
		byte[] msgType = list.getFirst().getMsgHeader_type();
		byte[] msgLen = list.getFirst().getMsgHeader_length();
		byte[] msgTime = list.getLast().getMsgHeader_time();
		//System.out.println("msgBean size = "+list.size());
		return new Message(msgStream,msgType[0],msgLen,msgTime,baos.toByteArray());
	}
}
