package pers.walty.wq_rtmp.server.message;

import pers.walty.wq_rtmp.server.util.ByteUtil;

public class MsgContent {
	private byte[] content;
	private int index;
	public MsgContent(byte[] content){
		this.content = content;
		this.index = 0;
	}
	
	public int getIndex() {
		return index;
	}
	
	public byte[] nextType(){
		byte[] reB = ByteUtil.subByteArray(content, index, index+1);
		index = index + 2;
		return reB;
	}
	
	public byte next(){
		return content[index++];
	}
	
	public int getLen(){
		return content.length;
	}
	
	public byte getNext(){
		return content[index];
	}
	
	public void mvIndex(){
		this.index++;
	}
	
	public byte[] getContent() {
		return content;
	}
	
	public byte[] subContent(int start,int len){
		return ByteUtil.subByteArray(content, start, start + len - 1);
	}
	
	public boolean isFull(){
		return index+1>=this.getLen();
	}
	
	public byte[] subContAndMvIndex(int len){
		int start = this.index;
		byte[] reB = this.subContent(start,len);
		this.index = this.index + len;
		/*
		if(this.isFull()){
			return null;
		}*/
		
		return reB;
	}
}
