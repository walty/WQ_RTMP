package pers.walty.wq_rtmp.server.message.chunk;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import pers.walty.wq_rtmp.server.debug.Log;
import pers.walty.wq_rtmp.server.message.MsgBean;
import pers.walty.wq_rtmp.server.message.MessageFactory;
import pers.walty.wq_rtmp.server.util.ByteUtil;

public class ChunkFactory {
	public static final int DATA_LEN = 2048;
	private Map<Integer,MsgHeaderBean> header;
	private int nowTimestamp = 0;
	
	int csidLen = 1
			,msgHeaderLen = 11
			,extendTimeLen = 0
			,dataLen=128
			,realDataLen=128
			,realMsgHeaderLen =11
			,bbLen = 0
			,contentLen=0;
			
	byte fmt = 0;
	byte[] csidBuffer = null
		,msgHeaderBuffer = null
		,msgHeaderTimeBuffer = null
		,msgHeaderLengthBuffer = null
		,msgHeaderTypeIdBuffer = null
		,msgHeaderStreamIdBuffer = null
		,extendTimeBuffer = null
		,dataBuffer = null;
	List<Byte> byteBuffer = null;
	MessageFactory msgFactory;
	
	public ChunkFactory(MessageFactory msgFactory){
		this.msgFactory = msgFactory;
		initAll();
		this.header = new HashMap<Integer,MsgHeaderBean>();
	}
	
	//after make a msg.
	private void initAll(){
		csidLen = 1;
		msgHeaderLen = 11;
		extendTimeLen = 0;
		dataLen=128;
		realDataLen=128;
		realMsgHeaderLen =11;
		fmt = 0;
		csidBuffer = null;
		msgHeaderBuffer = null;
		msgHeaderTimeBuffer = null;
		msgHeaderLengthBuffer = null;
		msgHeaderTypeIdBuffer = null;
		msgHeaderStreamIdBuffer = null;
		extendTimeBuffer = null;
		dataBuffer = null;
		byteBuffer = new ArrayList<Byte>();
		bbLen = 0;
		nowTimestamp = 0;
	}
	
	public void setRealDataLen(int realDataLen){
		//if(realDataLen > DATA_LEN){
		//	this.realDataLen = DATA_LEN;
		//}else{
			this.realDataLen = realDataLen;
		//}
	}
	
	public int getRealDataLen() {
		return realDataLen;
	}
	
	public void builder(BufferedInputStream bis,MessageFactory msgFactory) throws IOException, InterruptedException{
		int contentLen = bis.available();
		Map<byte[],MsgBean> saveChunk = msgFactory.getSaveChunk();
		do{
			BasicChunk bc = new BasicChunk();
			byte b = (byte)bis.read();
			byte fmt = ChunkBuilder.makeFmt(b);
			bc.setFmt(fmt);
			bc.setCsid(ChunkBuilder.makeCSID(b,bis));
			byte[] headerbuffer= ChunkBuilder.makeMsgHeader(fmt, bis);
			try{
				makeMsgHeader(bc,headerbuffer);
			}catch (NullPointerException e){
				System.out.println(ByteUtil.bytes2Str(headerbuffer == null ? new byte[]{0} : headerbuffer));
				System.out.println(fmt);
				System.out.println(ByteUtil.bytes2Str(bc.getCsid()));
				msgFactory.close();
				return ;
			}
			
			int extendTimeLen = ByteUtil.bytesEquals(bc.getMsgHeader_time(), new byte[]{-128,-128,-128})?4:0;
			if(extendTimeLen > 0){
				byte[] bArr = new byte[extendTimeLen];
				bis.read(bArr);
				bc.setExtendTime(bArr);
			}
			int msgLen = ByteUtil.bytesToInt(bc.getMsgHeader_length());
			int nowLen = saveChunk.containsKey(bc.getMsgHeader_stream())?saveChunk.get(bc.getMsgHeader_stream()).getNowLen():0;
			int deLen = msgLen - nowLen;
			//int dataLen = msgLen < realDataLen ? msgLen : realDataLen > deLen ? deLen : realDataLen;
			int dataLen = realDataLen < deLen ? realDataLen : deLen;
			dataLen = dataLen < 0 ? 0 : dataLen;
			if(bc.getFmt() == 0){
				nowTimestamp = ByteUtil.bytesToInt(bc.getMsgHeader_time());
			}else{
				nowTimestamp += ByteUtil.bytesToInt(bc.getMsgHeader_time());
			}
			bc.setMsgHeader_time(ByteUtil.intToBytes(nowTimestamp));
			//dataLen = msgLen;
			Log.append("{realDataLen = " +  realDataLen + "\n msgLen = "+msgLen+"\n nowLen =" + nowLen +"\n deLen = " + deLen + "\n dataLen = " + dataLen +"}\n");
			
			//byte[] b2Arr = new byte[dataLen];
			//bis.read(b2Arr);
			bc.setData(bigRead(bis,dataLen));
			//System.out.println(bc.toString());
			
			Log.append(bc.toString() + "\n");
			
			if(ByteUtil.bytesToInt(bc.getCsid()) != 2){
				if(saveChunk.containsKey(bc.getMsgHeader_stream())){
					saveChunk.get(bc.getMsgHeader_stream()).add(bc);
				}else{
					saveChunk.put(bc.getMsgHeader_stream(), new MsgBean(bc));
				}
				msgFactory.checkFull();
			}else{
				ControlChunk cc = new ControlChunk(bc,this,msgFactory);
				cc.control();
			}
			contentLen = bis.available();
		}while(contentLen > 0);
	}
	/*
	public BasicChunk add(byte b,int count){
		byteBuffer.add(b);
		bbLen++;
		
		if(bbLen == csidLen){
			fmt = ChunkBuilder.makeFmt(byteBuffer.get(0));
			csidLen = ChunkBuilder.makeCSIDLen(byteBuffer.get(0));
			if(bbLen == csidLen){
				msgHeaderLen = ChunkBuilder.makeMsgHeaderLen(fmt);
				//csidBuffer = ChunkBuilder.makeCSID(ByteUtil.subListToBArr(byteBuffer, 0, csidLen));
			}
		}else if(msgHeaderLen != 0 && bbLen == csidLen + msgHeaderLen ){
			msgHeaderBuffer = ByteUtil.subListToBArr(byteBuffer, csidLen, msgHeaderLen);
			switch(fmt){
			case ChunkBuilder.FMT_0:
				msgHeaderStreamIdBuffer = ChunkBuilder.makeMsgHeaderStreamId(msgHeaderBuffer);
			case ChunkBuilder.FMT_1:
				msgHeaderLengthBuffer = ChunkBuilder.makeMsgHeaderLength(msgHeaderBuffer);
				msgHeaderTypeIdBuffer = ChunkBuilder.makeMsgHeaderTypeId(msgHeaderBuffer);
			case ChunkBuilder.FMT_2:
				msgHeaderTimeBuffer = ChunkBuilder.makeMsgHeaderTime(msgHeaderBuffer);
				extendTimeLen = ChunkBuilder.makeNeedExtendTime(msgHeaderTimeBuffer)?4:0;
			case ChunkBuilder.FMT_3:
				break;
			}
			int msgLen = ByteUtil.bytesToInt(msgHeaderLengthBuffer);
			int deLen = msgLen - realDataLen * count;
			dataLen = msgLen < realDataLen ? msgLen : dataLen > deLen ? deLen : realDataLen;
			
		}else if(extendTimeLen != 0 && bbLen == csidLen + msgHeaderLen + extendTimeLen){
			extendTimeBuffer =  ByteUtil.subListToBArr(byteBuffer, csidLen + msgHeaderLen, extendTimeLen);
		}else if(dataLen != 0 && bbLen == csidLen + msgHeaderLen + extendTimeLen + dataLen){
			dataBuffer = ByteUtil.subListToBArr(byteBuffer, csidLen + msgHeaderLen + extendTimeLen, bbLen);
			//BasicChunk reChunk = new BasicChunk(fmt,csidBuffer,dataBuffer);
			BasicChunk reChunk = new BasicChunk(fmt,csidBuffer);
			switch(fmt){
			case ChunkBuilder.FMT_0:
				reChunk.setMsgHeader_stream(msgHeaderStreamIdBuffer);
			case ChunkBuilder.FMT_1:
				reChunk.setMsgHeader_length(msgHeaderLengthBuffer);
				reChunk.setMsgHeader_type(msgHeaderTypeIdBuffer);
			case ChunkBuilder.FMT_2:
				reChunk.setMsgHeader_time(msgHeaderTimeBuffer);
			case ChunkBuilder.FMT_3:
				if(null != extendTimeBuffer) reChunk.setExtendTime(extendTimeBuffer);
				break;
			}
			return reChunk;
		}
		return null;
	}*/
	
	public void makeMsgHeader(BasicChunk bc,byte[] msgHeaderBuffer){
		MsgHeaderBean mhb = null;
		/*
		if(header.containsKey(ByteUtil.bytesToInt(bc.getCsid()))){
			mhb = header.get(ByteUtil.bytesToInt(bc.getCsid()));
		}else{
			mhb = new MsgHeaderBean();
			header.put(ByteUtil.bytesToInt(bc.getCsid()), mhb);
		}*/
		switch(bc.getFmt()){
		case ChunkBuilder.FMT_0:
			mhb = new MsgHeaderBean();
			msgHeaderTimeBuffer = ChunkBuilder.makeMsgHeaderTime(msgHeaderBuffer);
			msgHeaderLengthBuffer =  ChunkBuilder.makeMsgHeaderLength(msgHeaderBuffer);
			msgHeaderTypeIdBuffer = ChunkBuilder.makeMsgHeaderTypeId(msgHeaderBuffer);
			msgHeaderStreamIdBuffer = ChunkBuilder.makeMsgHeaderStreamId(msgHeaderBuffer);
			mhb.setFmt0(msgHeaderTimeBuffer, msgHeaderLengthBuffer, msgHeaderTypeIdBuffer, msgHeaderStreamIdBuffer);
			header.put(ByteUtil.bytesToInt(bc.getCsid()), mhb);
			break;
		case ChunkBuilder.FMT_1:
			mhb = header.get(ByteUtil.bytesToInt(bc.getCsid()));
			msgHeaderTimeBuffer = ChunkBuilder.makeMsgHeaderTime(msgHeaderBuffer);
			msgHeaderLengthBuffer =  ChunkBuilder.makeMsgHeaderLength(msgHeaderBuffer);
			msgHeaderTypeIdBuffer = ChunkBuilder.makeMsgHeaderTypeId(msgHeaderBuffer);
			mhb.setFmt1(msgHeaderTimeBuffer, msgHeaderLengthBuffer, msgHeaderTypeIdBuffer);
			break;
		case ChunkBuilder.FMT_2:
			mhb = header.get(ByteUtil.bytesToInt(bc.getCsid()));
			msgHeaderTimeBuffer = ChunkBuilder.makeMsgHeaderTime(msgHeaderBuffer);
			mhb.setFmt2(msgHeaderTimeBuffer);
			break;
		case ChunkBuilder.FMT_3:
			mhb = header.get(ByteUtil.bytesToInt(bc.getCsid()));
			mhb.setFmt3();
			break;
		default :
		}
		
		mhb.setHeader(bc);
		
		
	}
	
	private byte[] bigRead(BufferedInputStream bis,int size) throws IOException, InterruptedException  {
		byte[] reBytes = new byte[size];
		for(int i = 0 ; i < size ; i++){
			reBytes[i] = (byte)bis.read();
		}
		return reBytes;
	}
}

class MsgHeaderBean {
	private byte[] time;
	private byte[] length;
	private byte[] type;
	private byte[] stream;
	MsgHeaderBean(){}
	public byte[] getTime() {
		return time;
	}
	public void setTime(byte[] time) {
		this.time = time;
	}
	public byte[] getLength() {
		return length;
	}
	public void setLength(byte[] length) {
		this.length = length;
	}
	public byte[] getType() {
		return type;
	}
	public void setType(byte[] type) {
		this.type = type;
	}
	public byte[] getStream() {
		return stream;
	}
	public void setStream(byte[] stream) {
		this.stream = stream;
	}
	public void setFmt0(byte[] time,byte[] length,byte[] type,byte[] stream){
		this.setTime(time);
		this.setLength(length);
		this.setType(type);
		this.setStream(stream);
	}
	public void setFmt1(byte[] time,byte[] length,byte[] type){
		this.setTime(time);
		this.setLength(length);
		this.setType(type);
	}
	public void setFmt2(byte[] time){
		this.setTime(time);
	}
	public void setFmt3(){}
	public void setHeader(BasicChunk bc){
		bc.setMsgHeader_time(this.getTime());
		bc.setMsgHeader_length(this.getLength());
		bc.setMsgHeader_type(this.getType());
		bc.setMsgHeader_stream(this.getStream());
	}
}
 