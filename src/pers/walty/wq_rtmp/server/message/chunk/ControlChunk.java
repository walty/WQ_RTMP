package pers.walty.wq_rtmp.server.message.chunk;

import java.util.Map;
import pers.walty.wq_rtmp.server.message.MessageFactory;
import pers.walty.wq_rtmp.server.message.MsgBean;
import pers.walty.wq_rtmp.server.util.ByteUtil;

public class ControlChunk {
	public static final int MSG_TYPE_1 = 1;
	public static final int MSG_TYPE_2 = 2;
	public static final int MSG_TYPE_3 = 3;
	public static final int MSG_TYPE_5 = 5;
	public static final int MSG_TYPE_6 = 6;
	private ChunkFactory chunkFactory;
	private MessageFactory msgFactory;
	private BasicChunk bc;
	public ControlChunk(BasicChunk bc,ChunkFactory chunkFactory,MessageFactory msgFactory){
		this.bc = bc;
		this.chunkFactory = chunkFactory;
		this.msgFactory = msgFactory;
	}
	
	public void control(){
		int type = ByteUtil.bytesToInt(bc.getMsgHeader_type());
		switch(type){
		case MSG_TYPE_1:
			chunkFactory.setRealDataLen(ByteUtil.bytesToInt(bc.getData()));
			break;
		case MSG_TYPE_2:
			Map<byte[],MsgBean> map = msgFactory.getSaveChunk();
			map.remove(bc.getData());
			break;
		case MSG_TYPE_3:
			break;
		case MSG_TYPE_5:
			break;
		case MSG_TYPE_6:
			break;
		}
	}
	
}
