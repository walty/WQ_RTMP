package pers.walty.wq_rtmp.server.message.chunk;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class SendChunk {
	private BasicChunk bc;
	public SendChunk(BasicChunk bc){
		this.bc = bc;
	}
	
	public byte[] getContent() {
		List<Byte> list = new LinkedList<Byte>();
		//list.add(bc.getFmt());
		mkList(list,bc.getCsid());
		mkList(list,bc.getMsgHeader_time());
		mkList(list,bc.getMsgHeader_length());
		mkList(list,bc.getMsgHeader_type());
		mkList(list,bc.getMsgHeader_stream());
		if(bc.getExtendTime() != null){
			mkList(list, bc.getExtendTime());
		}
		mkList(list, bc.getData());
		return listToBytes(list);
	}
	
	private void mkList(List<Byte> list,byte[] b){
		for (byte c : b) {
			list.add(c);
		}
	}
	
	private byte[] listToBytes(List<Byte> list){
		byte[] reB = new byte[list.size()];
		Iterator<Byte> iterator = list.iterator();
		int i = 0;
		while(iterator.hasNext()){
			reB[i++] = iterator.next();
		}
		return reB;
	}
}
