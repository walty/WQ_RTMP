package pers.walty.wq_rtmp.server.message.chunk;

import pers.walty.wq_rtmp.server.util.ByteUtil;

public class BasicChunk {
	protected byte fmt;
	protected byte[] csid
			,msgHeader_time
			,msgHeader_length
			,msgHeader_type
			,msgHeader_stream
			,extendTime
			,data;
		
	public BasicChunk(){
		
	}
	
	public BasicChunk(byte fmt,byte[] csid){
		this.fmt = fmt;
		this.csid = csid;
	}

	public String toString(){
		return "[ftm = "+fmt
				+"\n csid = " + ByteUtil.bytesToInt(csid)
				+"\n msgHeader_time = " + ByteUtil.bytesToInt(msgHeader_time)
				+"\n msgHeader_length = " + ByteUtil.bytesToInt(msgHeader_length)
				+"\n msgHeader_type = " + ByteUtil.bytesToInt(msgHeader_type)
				+"\n msgHeader_stream = " + ByteUtil.bytesToInt(msgHeader_stream)
				+"\n extendTime = " + ByteUtil.bytesToInt(extendTime)
				+ "]";
	}
	public byte getFmt() {
		return fmt;
	}

	public void setFmt(byte fmt) {
		this.fmt = fmt;
	}

	public byte[] getCsid() {
		return csid;
	}

	public void setCsid(byte[] csid) {
		this.csid = csid;
	}

	public byte[] getMsgHeader_time() {
		return msgHeader_time;
	}

	public void setMsgHeader_time(byte[] msgHeader_time) {
		this.msgHeader_time = msgHeader_time;
	}

	public byte[] getMsgHeader_length() {
		return msgHeader_length;
	}

	public void setMsgHeader_length(byte[] msgHeader_length) {
		this.msgHeader_length = msgHeader_length;
	}

	public byte[] getMsgHeader_type() {
		return msgHeader_type;
	}

	public void setMsgHeader_type(byte[] msgHeader_type) {
		this.msgHeader_type = msgHeader_type;
	}

	public byte[] getMsgHeader_stream() {
		return msgHeader_stream;
	}

	public void setMsgHeader_stream(byte[] msgHeader_stream) {
		this.msgHeader_stream = msgHeader_stream;
	}

	public byte[] getExtendTime() {
		return extendTime;
	}
	
	public void setExtendTime(byte[] extendTime) {
		this.extendTime = extendTime;
	}
	
	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

}
