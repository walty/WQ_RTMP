package pers.walty.wq_rtmp.server.message.chunk;

import java.util.HashMap;
import java.util.Map;

public class Chunk {
	protected byte[] content;
	protected Map<String,byte[]> map;
	protected int type;
	
	public Chunk(byte[] content,int type){
		this.setContent(content);
		this.map = new HashMap<String,byte[]>();
		this.type = type;
	}
	
	public Chunk(byte[] content){
		this.setContent(content);
		this.map = new HashMap<String,byte[]>();
	}
	
	protected Chunk setType(int type){
		this.type = type;
		return this;
	}
	
	protected Chunk setContent(byte[] content){
		this.content = content;
		return this;
	}
	
	protected Chunk setMap(Map<String,byte[]> map){
		this.map = map;
		return this;
	}
	
	protected int getType(){
		return this.type;
	}
	
	public byte[] getContent(){
		return this.content;
	}
	
	public Map<String,byte[]> getMap(){
		return this.map;
	}

}
