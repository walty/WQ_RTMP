package pers.walty.wq_rtmp.server.message.chunk;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Random;

import pers.walty.wq_rtmp.server.util.ByteUtil;
import pers.walty.wq_rtmp.server.util.amf.ObjAmf;



public class ChunkBuilder {
	public static final byte RTMP_VERSION = 3;
	public static final int SHAKE_CHUNK_LENGTH = 1536;
	public static int CHUNK_SIZE = 128;
	public static final int MSG_TYPE_0 = 0;
	public static final int MSG_TYPE_1 = 1;
	public static final int MSG_TYPE_2 = 2;
	public static final int MSG_TYPE_3 = 3;
	public static final int CSID_0 = 0;//CSIDlen = 2字节
	public static final int CSID_1 = 1;//CSIDlen = 3字节
	public static final int CSID_2 = 3;//Control = 1字节
	public static final int FMT_0 = 0;
	public static final int FMT_1 = 1;
	public static final int FMT_2 = 2;
	public static final int FMT_3 = 3;
	
	public static ShakeChunk firstShake(byte[] b){
		byte[] c0_b = {b[0]};
		return makeChunkC0(c0_b,makeChunkC1(ByteUtil.subByteArray(b, 1, SHAKE_CHUNK_LENGTH)));
	}
	
	public static ShakeChunk reFirstShake(ShakeChunk c0){
		if(c0.getContent()[0] == RTMP_VERSION){
			ShakeChunk s0 = makeChunkS0();
			s0.setNextChunk(makeChunkS1());
			return s0;
		}
		return null;
	}

	
	public static ShakeChunk secondShake(byte[] b){
		return makeChunkC2(b);
	}
	
	/**
	 * @param s1 c2 用于校验是否正确
	 * @param c1 用于构建s2
	 * */
	public static ShakeChunk reSecondShake(ShakeChunk s1,ShakeChunk c2,ShakeChunk c1){
		if(Arrays.equals(s1.getMap().get("time"),c2.getMap().get("time"))){
			return makeChunkS2(c1);
		}
		return null;
	}
	
	public static ShakeChunk makeChunkC0(byte[] b,ShakeChunk next){
		return new ShakeChunk(b,ShakeChunk.C0,next);
	}
	
	public static ShakeChunk makeChunkC1(byte[] b){
		return new ShakeChunk(b,ShakeChunk.C1,null);
	}
	
	public static ShakeChunk makeChunkC2(byte[] b){
		return new ShakeChunk(b,ShakeChunk.C2,null);
	}
	
	public static ShakeChunk makeChunkS0(){
		byte[] b = {RTMP_VERSION};
		return new ShakeChunk(b,ShakeChunk.S0,null);
	}
	
	public static ShakeChunk makeChunkS1(){
		byte[] rb = new byte[SHAKE_CHUNK_LENGTH-8];
		Random random = new Random();
		random.nextBytes(rb);
		byte[] time = {0,0,0,0};
		byte[] zero = {0,0,0,0};
		byte[] b = ByteUtil.concatAll(time, zero,rb);
		return new ShakeChunk(b,ShakeChunk.S1,null);
	}
	
	public static ShakeChunk makeChunkS2(ShakeChunk c1){
		byte[] zero = {0,0,0,0};
		byte[] b = ByteUtil.concatAll(c1.getMap().get("time"), zero,c1.getMap().get("random bytes"));
		return new ShakeChunk(b,ShakeChunk.S2,null);
	}
	
	/**
	 * @param content please sub CSID
	 * @param fmt input the chunk fmt
	 * @return byte[] ,need the Chunk set Map
	 * */
	public static byte[] makeMsgHeader(byte[] content,int fmt){
		byte[] reByte;
		switch(fmt){
		case MSG_TYPE_0:
			reByte = ByteUtil.subByteArray(content, 0, 10);
			break;
		case MSG_TYPE_1:
			reByte = ByteUtil.subByteArray(content, 0, 6);
			break;
		case MSG_TYPE_2:
			reByte = ByteUtil.subByteArray(content,0,2);
			break;
		case MSG_TYPE_3:
			reByte = null;
			break;
		default:;
			reByte = null;
		}
		return reByte;
	}
	
	/**
	 * @maybeCSID please sub content [0,3]
	 * @return byte[] CSID
	 * */
	public static byte makeCSIDType(byte b){
		byte flag = (byte) ((b << 2) >>2);//remove fmt
		return flag;
	}
	
	public static byte[] makeMsgHeader(byte fmt,BufferedInputStream bis) throws IOException{
		byte[] reByte;
		switch(fmt){
		case FMT_0:
			reByte = new byte[11];
			bis.read(reByte);
			break;
		case FMT_1:
			reByte = new byte[7];
			bis.read(reByte);
			break;
		case FMT_2:
			reByte = new byte[3];
			bis.read(reByte);
			break;
		case FMT_3:
		default: 
			reByte = null;
		}
		return reByte;
	}
	
	public static byte[] makeCSID(byte b , BufferedInputStream bis) throws IOException{
		byte flag = makeCSIDFlag(b);//remove fmt
		byte[] reByte;
		switch(flag){
		case 0:
			reByte = new byte[2];
			reByte[0] = flag;
			reByte[1] = (byte)bis.read();
			break;
		case 1:
			reByte = new byte[3];
			reByte[0] = flag;
			reByte[1] = (byte)bis.read();
			reByte[2] = (byte)bis.read();
			break;
		default:
			reByte = new byte[1];
			reByte[0] = flag;
			break;
		}
		return reByte;
	}
	
	public static byte makeCSIDFlag(byte b){
		return (byte) (b & 0x3f);//remove fmt
	}
	
	public static byte makeFmt(byte b){
		//byte reByte = (byte) (b >> 6);
		byte reByte = (byte) ((b & 0xff) >>> 6);
		return reByte;
	}
	
	public static int makeMsgHeaderLen(int fmt){
		int reInt;
		switch(fmt){
		case MSG_TYPE_0:
			reInt = 11;
			break;
		case MSG_TYPE_1:
			reInt = 7;
			break;
		case MSG_TYPE_2:
			reInt = 3;
			break;
		case MSG_TYPE_3:
			reInt = 0;
			break;
		default:
		reInt = 0;
		}
		return reInt;
	}
	
	public static int makeCSIDLen(byte type){
		int len;
		switch(type){
		case CSID_0:len=2;break;
		case CSID_1:len=3;break;
		default:len=1;
		}
		return len;
	}
	
	public static byte[] makeMsgHeaderTime(byte[] msgHeader){
		byte[] reB = new byte[3];
		for(int i = 0 ; i < 3; i++){
			reB[i] = msgHeader[i];
		}
		return reB;
	}
	
	public static byte[] makeMsgHeaderLength(byte[] msgHeader){
		byte[] reB = new byte[3];
		for(int i = 0 ; i < 3 ; i++){
			reB[i] = msgHeader[3+i];
		}
		return reB;
	}
	
	public static byte[] makeMsgHeaderTypeId(byte[] msgHeader){
		byte[] reB = new byte[1];
		reB[0] = msgHeader[6];
		return reB;
	}
	
	public static byte[] makeMsgHeaderStreamId(byte[] msgHeader){
		byte[] reB = new byte[4];
		for(int i = 0 ; i < 4 ; i++){
			reB[i] = msgHeader[7+i];
		}
		return reB;
	}
	
	public static boolean makeNeedExtendTime(byte[] msgHeaderTime){
		for(int i = 0 ; i < 3 ; i++){
			if(msgHeaderTime[i] != -128){
				return false;
			}
		}
		return true;
	}
	
	/*
	public static void makeMsgHeader(BasicChunk bc,byte[] msgHeaderBuffer){
		switch(bc.getFmt()){
		case ChunkBuilder.FMT_0:
			bc.setMsgHeader_stream(ChunkBuilder.makeMsgHeaderStreamId(msgHeaderBuffer));
		case ChunkBuilder.FMT_1:
			bc.setMsgHeader_length( ChunkBuilder.makeMsgHeaderLength(msgHeaderBuffer));
			bc.setMsgHeader_type( ChunkBuilder.makeMsgHeaderTypeId(msgHeaderBuffer));
		case ChunkBuilder.FMT_2:
			bc.setMsgHeader_time( ChunkBuilder.makeMsgHeaderTime(msgHeaderBuffer));
		case ChunkBuilder.FMT_3:
			break;
		default:
			System.out.println("Experit fmt");
			break;
		}
	}*/
	
	public static BasicChunk makeAckChunk(int length){
		return makeControlChunk((byte)3,ByteUtil.intToBytes(length));
	}
	
	public static BasicChunk makeWinAckSizeChunk(byte[] data){
		return makeControlChunk((byte)5,data);
	}
	
	public static BasicChunk makeSetPeerBankwith(byte[] data){
		return makeControlChunk((byte)6,data);
	}
	
	public static BasicChunk makeUserControlMsg(byte eType,byte[] eData){
		return makeUserControlMsg(new byte[]{0,eType},eData);
	}
	
	public static BasicChunk makeUserControlMsg(byte[] eType,byte[] eData){
		if(eType.length < 2){
			eType = ByteUtil.fillBytes(2, eType);
		}
		byte[] data = ByteUtil.concatAll(eType, eData);
		return makeControlChunk((byte)4,data);
	}
	
	private static BasicChunk makeControlChunk(byte type,byte[] data){
		BasicChunk bc = new BasicChunk();
		bc.setCsid(new byte[]{(byte)2});
		bc.setMsgHeader_time(new byte[]{0,0,0});
		bc.setMsgHeader_stream(new byte[]{0,0,0,0});
		bc.setMsgHeader_length(ByteUtil.subByteArray(ByteUtil.intToBytes(data.length), 1, 3));
		bc.setMsgHeader_type(new byte[]{type});
		bc.setData(data);
		return bc;
	}
	
	public static BasicChunk amfObj2BC(ObjAmf objAmf){
		BasicChunk bc = new BasicChunk();
		bc.setCsid(new byte[]{2});
		byte[] content = objAmf.makeBytes();
		bc.setMsgHeader_length(ByteUtil.subBytes(3, ByteUtil.intToBytes(content.length)));
		bc.setMsgHeader_stream(new byte[]{0,0,0,0});
		bc.setMsgHeader_time(new byte[]{0,0,0});
		bc.setMsgHeader_type(new byte[]{(byte)objAmf.getType()});
		//System.out.println(content.length + "][" + objAmf.getType());
		bc.setData(content);
		return bc;
	}
	
	/*
	public static BasicChunk[] sliceMsg(MsgContent msgCon,int dataSize,byte[] basicHeader){
		BasicChunk[] reBC = null;
		List<BasicChunk> bl = new LinkedList();
		if(msgCon.getLen()<dataSize){
			//BasicChunk = 
			reBC = {};
		}
		return reBC;
	}*/
}
