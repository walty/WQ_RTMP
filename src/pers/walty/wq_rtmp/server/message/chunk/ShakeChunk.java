package pers.walty.wq_rtmp.server.message.chunk;

import pers.walty.wq_rtmp.server.util.ByteUtil;


public class ShakeChunk extends Chunk {
	public static final int C0 = 0;
	public static final int C1 = 1;
	public static final int C2 = 2;
	public static final int S0 = 3;
	public static final int S1 = 4;
	public static final int S2 = 5;
	
	private ShakeChunk nextChunk;
	
	public ShakeChunk(byte[] content, int type,ShakeChunk nextChunk) {
		super(content, type);
		// TODO Auto-generated constructor stub
		this.nextChunk = nextChunk;
		init();
	}
	
	private void init(){
		switch(type){
		case C0:case S0:
			map.put("version", content);
			break;
		case C1:case S1:
			map.put("time", ByteUtil.subByteArray(content, 0, 3));
			map.put("zero", ByteUtil.subByteArray(content, 4, 7));
			map.put("random bytes", ByteUtil.subByteArray(content, 8, ChunkBuilder.SHAKE_CHUNK_LENGTH-1));
			break;
		case S2 : case C2 :
			map.put("time", ByteUtil.subByteArray(content, 0, 3));
			map.put("time2", ByteUtil.subByteArray(content, 4, 7));
			map.put("random bytes", ByteUtil.subByteArray(content, 8, ChunkBuilder.SHAKE_CHUNK_LENGTH-1));
			break;
		}
	}
	
	public ShakeChunk setNextChunk(ShakeChunk sc){
		this.nextChunk = sc;
		return this;
	}
	public ShakeChunk getNextChunk(){
		return this.nextChunk;
	}

}
