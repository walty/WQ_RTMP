package pers.walty.wq_rtmp.server.message;

import java.util.List;

public class MessageBuilder {
	public static byte[] subListToBArr(List<Byte> list,int start,int length){
		int len = list.size();
		if(start + len < length){
			return null;
		}
		byte[] reB = new byte[len];
		for(int i = start,j=0 ; i < start + len ; i++,j++){
			reB[j] = list.get(i);
		}
		return reB;
	}
}
