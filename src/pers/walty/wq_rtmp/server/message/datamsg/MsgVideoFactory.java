package pers.walty.wq_rtmp.server.message.datamsg;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import pers.walty.wq_rtmp.server.message.Message;
import pers.walty.wq_rtmp.server.util.ByteUtil;

public class MsgVideoFactory {
	public MsgVideoFactory() {
		// TODO Auto-generated constructor stub
	}
	
	public byte[] setAvcSequenceHeader(Message msg){
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] content = msg.getContent();
		int len = ByteUtil.bytesToInt(ByteUtil.subByteArray(content, 11, 12));
		try {
			//sps
			baos.write(new byte[]{0x00,0x00,0x00,0x01});
			baos.write(ByteUtil.subByteArray(content,13,12+len));
			//pps
			baos.write(new byte[]{0x00,0x00,0x00,0x01});
			int len2 = ByteUtil.bytesToInt(ByteUtil.subByteArray(content, 14+len, 15+len));
			baos.write(ByteUtil.subByteArray(content,16+len,15+len+len2));
			baos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return baos.toByteArray();
	}
	
	public H264Frame decodeMsg2Frame(Message msg){
		H264Frame reFrame = null;
		try {
			reFrame = this.dealData(msg);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return reFrame;
	}
	/*
	public void add(Message msg){
		System.out.println("Video add");
		try {
			if(this.index == 0 ){
				this.setAvcSequenceHeader(msg.getContent());
			}else{
				this.dealData(msg.getContent());
				if(this.index == 400){
					this.saveAll();
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.index++;
		System.out.println("Video add over index=" + index);
	}
	
	private void saveAll() {
		// TODO Auto-generated method stub
		System.out.println("save all");
		//new SaveWorker(new DataMsgVideo(baos.toByteArray()),name,index+"");
	}

	private void setAvcSequenceHeader(byte[] content) throws IOException{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		System.out.println("Video setAvcSequenceHeader");
		int len = ByteUtil.bytesToInt(ByteUtil.subByteArray(content, 11, 12));
		//sps
		baos.write(new byte[]{0x00,0x00,0x00,0x01});
		baos.write(ByteUtil.subByteArray(content,13,12+len));
		videoList.add(new H264Frame(msg.getMsgTime(),baos.toByteArray(),H264Frame.I_FRAME));
		
		//pps
		baos.close();
		baos = new ByteArrayOutputStream();
		baos.write(new byte[]{0x00,0x00,0x00,0x01});
		int len2 = ByteUtil.bytesToInt(ByteUtil.subByteArray(content, 14+len, 15+len));
		baos.write(ByteUtil.subByteArray(content,16+len,15+len+len2));
		videoList.add(new H264Frame(msg.getMsgTime(),baos.toByteArray(),H264Frame.I_FRAME));
		baos.close();
	}*/
	
	private H264Frame dealData(Message msg) throws IOException{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] data = msg.getContent();
		int type;
		if(data[0] == 0x17){
			baos.write(new byte[]{0x00,0x00,0x00,0x01});
			type = H264Frame.I_FRAME;
		}else{
			baos.write(new byte[]{0x00,0x00,0x01});
			type = H264Frame.P_FRAME;
		}
		
		baos.write(data, 9, data.length - 9);
		H264Frame frame = new H264Frame(baos.toByteArray(), type);
		frame.setDtsTime(msg.getMsgTime());
		int time = ByteUtil.bytesToInt(ByteUtil.subByteArray(data, 2, 4));
		frame.setCompositionTime(time);
		return frame;
	}
	
}
