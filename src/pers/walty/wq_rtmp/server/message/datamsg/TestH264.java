package pers.walty.wq_rtmp.server.message.datamsg;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;

public class TestH264 {
	public static final int MAX_COUNT = 120;
	private byte[] h264First;
	private LinkedList list;
	private int count;
	public TestH264() {
		// TODO Auto-generated constructor stub
		list = new LinkedList();
	}
	
	public void setH264First(byte[] h264First) {
		this.h264First = h264First;
	}
	
	public void add(Frame frame) throws IOException{
		list.add(frame);
		if(count == MAX_COUNT){
			new SaveWorker(new DataMsgVideo(allByteArray()),"Walty","120");
		}
		count++;
	}
	
	private byte[] allByteArray() throws IOException{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		baos.write(h264First);
		Iterator<Frame> i = list.iterator();
		while(i.hasNext()){
			baos.write(i.next().getData());
		}
		return baos.toByteArray();
	}
	
	
	
	
}
