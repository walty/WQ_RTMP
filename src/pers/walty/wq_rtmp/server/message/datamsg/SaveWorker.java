package pers.walty.wq_rtmp.server.message.datamsg;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class SaveWorker {
	public static final String SAVE_DATA_PATH = "./videoTemp";
	
	private FileOutputStream fos;

	
	public SaveWorker(DataMsg dm,String name,String index){
		try {
			File file = new File(SAVE_DATA_PATH + "/" + name + "/" + index + dm.getSuffix());
			SaveWorker.checkFile(file);
			fos = new FileOutputStream(file);
			this.writeData(dm.getData());
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public static void checkFile(File file) throws IOException{
		if(!file.getParentFile().isDirectory()){
			file.getParentFile().mkdir();
		}
		if(!file.exists()){
			file.createNewFile();
		}else{
			file.delete();
			file.createNewFile();
		}
	}
	
	private void writeData(byte[] data) throws IOException{
		this.fos.write(data);
	}
	
}
