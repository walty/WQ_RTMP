package pers.walty.wq_rtmp.server.message.datamsg;


public class Frame {
	protected byte[] data;
	protected int dtsTime;
	
	public Frame(byte[] data) {
		// TODO Auto-generated constructor stub
		setData(data);
	}
	
	public void setData(byte[] data) {
		this.data = data;
	}
	
	public byte[] getData() {
		return data;
	}
	
	public void setDtsTime(int dtsTime) {
		this.dtsTime = dtsTime;
	}
	
	public int getDtsTime() {
		return dtsTime;
	}
}
