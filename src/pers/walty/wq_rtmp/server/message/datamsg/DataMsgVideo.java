package pers.walty.wq_rtmp.server.message.datamsg;

import pers.walty.wq_rtmp.server.message.Message;

public class DataMsgVideo extends DataMsg{
	
	
	
	public DataMsgVideo(Message msg) {
		// TODO Auto-generated constructor stub
		super(msg.getContent());
		this.setSuffix(".264");
	}
	
	public DataMsgVideo(byte[] data){
		super(data);
		this.setSuffix(".264");
		
	}
	

}
