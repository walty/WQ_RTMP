package pers.walty.wq_rtmp.server.message.datamsg;

import pers.walty.wq_rtmp.server.message.Message;

public class DataMsgVoice extends DataMsg{
	
	
	public DataMsgVoice(Message msg) {
		// TODO Auto-generated constructor stub
		super(msg.getContent());
		this.setSuffix(".m4a");
	}
	
	public DataMsgVoice(byte[] data){
		this.setSuffix(".aac");
		this.setData(data);
	}
}
