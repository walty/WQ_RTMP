package pers.walty.wq_rtmp.server.message.datamsg;

public class H264Frame extends Frame{
	public static final int I_FRAME = 0 ;
	public static final int P_FRAME = 1 ;
	public static final int B_FRAME = 2 ;

	private int h264FrameType;
	private int compositionTime;
	
	public H264Frame(byte[] data,int type) {
		super(data);
		// TODO Auto-generated constructor stub
		setH264FrameType(type);
	}
	
	public void setH264FrameType(int h264FrameType) {
		/*
		if(h264FrameType == P_FRAME){
			if(this.data[5] == (byte)0x9E || this.data[5] == (byte)0x9f){
				h264FrameType = B_FRAME;
			}
		}*/
		this.h264FrameType = h264FrameType;
	}
	
	public void setCompositionTime(int compositionTime) {
		this.compositionTime = compositionTime;
	}
	
	public int getCompositionTime() {
		return compositionTime;
	}
	
	public int getH264FrameType() {
		return h264FrameType;
	}
	
}
