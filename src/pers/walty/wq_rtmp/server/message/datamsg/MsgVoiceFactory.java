package pers.walty.wq_rtmp.server.message.datamsg;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import pers.walty.wq_rtmp.server.message.Message;
import pers.walty.wq_rtmp.server.util.ByteUtil;

public class MsgVoiceFactory {

	private byte audioObjectType;
	private byte sampleRateIndex;
	private byte channelConfig;
	
	public MsgVoiceFactory(){
	}
	
	public AacFrame decodeMsg2Frame(Message msg){
		AacFrame frame = new AacFrame(this.dealData(msg.getContent()));
		frame.setDtsTime(msg.getMsgTime());
		return frame;
	}
	
	/*
	public void add(Message msg){
		if(this.index == 0 ){
			this.setAacSequenceHeader(msg.getContent());
		}else{
			dmVoice.add(new AacFrame(this.dealData(msg.getContent())));
			if(this.index == 150){
				this.saveAll();
			}
		}
		this.index++;
	}
	
	
	public void saveAll(){
		//System.out.println("save all");
		Iterator<AacFrame> i = this.dmVoice.iterator();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		while(i.hasNext()){
			try {
				baos.write(i.next().getData());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		new SaveWorker(new DataMsgVoice(baos.toByteArray()),name,index+"");
	}*/
	
	public byte[] dealData(byte[] data){
		// TODO Auto-generated method stub
		byte[] accHead = new byte[7];
		byte[] frameLength = ByteUtil.intToBytes(data.length + 5);
		accHead[0] = (byte) 0xff;
		accHead[1] = (byte) 0xf9;
		accHead[2] = (byte) (((byte)(audioObjectType << 6)) | ((byte)((sampleRateIndex & 0x0f) << 2)) | (byte)((channelConfig & 0xff) >>> 2));
		accHead[3] = (byte) ((byte) (channelConfig << 6) | (byte)(frameLength[2]>>3));
		accHead[4] = (byte) ((byte) (frameLength[2]<<5) | (byte)(frameLength[3]>>3));
		accHead[5] = (byte) (frameLength[3]<<5);
		accHead[5] |= (byte) 0x1f;
		accHead[6] = (byte) 0xfc;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			baos.write(accHead);
			baos.write(data, 2, data.length-2);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 		return baos.toByteArray();
	}
	
	public void setAacSequenceHeader(byte[] content){
		audioObjectType = MsgVoiceFactory.initAudioObjectType(content);
		sampleRateIndex = MsgVoiceFactory.initSampleRateIndex(content);
		channelConfig = MsgVoiceFactory.initChannelConfig(content);
		//System.out.println("AacSequenceHeader [content ="+ByteUtil.bytes2Str(content)+"]");
		//System.out.println("AacSequenceHeader [audioObjType = " + audioObjectType + "] [sampleRateIndex =" + sampleRateIndex + "] [ channelConfig =" + channelConfig + "]");	
	}
	
	private static byte initAudioObjectType(byte[] content){
		return (byte) ((byte) (content[2]>>3) - 0x01);
	}
	
	private static byte initChannelConfig(byte[] content) {
		// TODO Auto-generated method stub
		return (byte) ((byte) (content[3]<<1) >>> 4);
	}

	private static byte initSampleRateIndex(byte[] content){
		byte f0 = (byte)((byte)(content[2] & 0x07) << 1) ;
		byte f1 = (byte)((byte)((content[3] & 0xff) >>> 7));
		//System.out.println("initSampleRateIndex [f0 =" + f0 + "][f1 = "+f1+"][content2 = " +content[2]+"][content3 = "+content[3]);
		return (byte)(f0|f1);
	} 
}
