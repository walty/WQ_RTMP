package pers.walty.wq_rtmp.server.message.datamsg;

public class DataMsg {
	private byte[] data;
	private String suffix;
	
	public DataMsg(byte[] data){
		this.setData(data);
	}
	
	public DataMsg(){
		
	}
	
	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}
	
	public String getSuffix() {
		return suffix;
	}
	
	public byte[] getData(){
		return this.data;
	}
	public void setData(byte[] data){
		this.data = data;
	}
}
