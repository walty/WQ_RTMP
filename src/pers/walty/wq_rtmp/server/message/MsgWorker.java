package pers.walty.wq_rtmp.server.message;

import java.io.IOException;
import java.util.List;
import pers.walty.wq_rtmp.server.hls.HlsFactory;
import pers.walty.wq_rtmp.server.message.chunk.BasicChunk;
import pers.walty.wq_rtmp.server.message.chunk.ChunkBuilder;
import pers.walty.wq_rtmp.server.message.datamsg.MsgVideoFactory;
import pers.walty.wq_rtmp.server.message.datamsg.MsgVoiceFactory;
import pers.walty.wq_rtmp.server.util.ByteUtil;
import pers.walty.wq_rtmp.server.util.amf.Amf0Builder;
import pers.walty.wq_rtmp.server.util.amf.DealAmf;
import pers.walty.wq_rtmp.server.util.amf.DealAmf0;
import pers.walty.wq_rtmp.server.util.amf.ObjAmf;
import pers.walty.wq_rtmp.server.util.amf.ObjAmf0;

public class MsgWorker {
	private MessageFactory msgFactory;
	private MsgVoiceFactory voiceFactory;
	private MsgVideoFactory videoFactory;
	private HlsFactory tsFactory;
	private boolean videoFirst;
	private boolean voiceFirst;
	private String name;
	
	public MsgWorker(MessageFactory msgFactory){
		this.msgFactory = msgFactory;
		this.name = "walty";
		videoFirst = true;
		voiceFirst = true;
	}
	
	public void working(Message msg){
		try {
			switch(msg.getMsgType()){
			case 9:
				//System.out.println("working 9");
				if(videoFirst){
					byte[] h264First = this.videoFactory.setAvcSequenceHeader(msg);
					//th.setH264First(h264First);
					this.tsFactory.setAvcSequenceHeader(h264First);
					videoFirst = false;
				}else{
					//th.add(this.videoFactory.decodeMsg2Frame(msg));
					this.tsFactory.pushH264Frame(this.videoFactory.decodeMsg2Frame(msg));
				}
				//this.videoFactory.add(msg);
				break;
			case 8:
				//System.out.println("working 8");
				if(voiceFirst){
					this.voiceFactory.setAacSequenceHeader(msg.getContent());
					voiceFirst = false;
				}else{
					this.tsFactory.pushAacFrame(this.voiceFactory.decodeMsg2Frame(msg));
				}
				break;
			default:
				ObjAmf objAmf = this.mkObj(msg);
				if(null != objAmf){
					this.dealList(objAmf);
				}
				break;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public ObjAmf mkObj(Message msg){
		MsgContent msgContent = new MsgContent(msg.getContent());
		List list = null;
		ObjAmf reObj = null;
		DealAmf da;
		switch(msg.getMsgType()){
		case 20:case 19:
			da = new DealAmf0();
			list = da.deal(msgContent);
			reObj = new ObjAmf0(list);
			reObj.setType(msg.getMsgType());
			break;
		case 17:case 16:case 15:
			System.out.println("Amf3");
			break;
		case 3:
			System.out.println(ByteUtil.bytesToInt(msg.getContent()));
			break;
		default:
			System.out.println("Experience msg type");
		}
		return reObj;
	}
	
	public void dealList(ObjAmf objAmf) throws IOException{
		switch(objAmf.getType()){
		case 20:case 17:
			commandMsg(objAmf);
			break;
		default:
			System.out.println("Experience msgType");
		}
	}
	
	private void commandMsg(ObjAmf objAmf) throws IOException{
		switch((String)objAmf.getValue(0)){
		case "connect":
			int len = msgFactory.getCf().getRealDataLen();
			byte[] winSize = ByteUtil.intToBytes(len);
			//makeWinAckSizeChunk
			BasicChunk bc = ChunkBuilder.makeWinAckSizeChunk(winSize);
			msgFactory.getRtmpSocket().pushBytes(bc);
			//makeSetPeerBankwith
			BasicChunk bc2 = ChunkBuilder.makeSetPeerBankwith(ByteUtil.concatAll(winSize, new byte[]{2}));
			msgFactory.getRtmpSocket().pushBytes(bc2);
			//makeUserControlMsg StreamBegin
			BasicChunk bc3 = ChunkBuilder.makeUserControlMsg((byte) 0, ByteUtil.intToBytes(1));
			msgFactory.getRtmpSocket().pushBytes(bc3);
			//mk _result
			ObjAmf objAmfT = Amf0Builder.mkResult((double)(objAmf.getValue(1)));
			objAmfT.setType(20);
			this.name = (String)((List)objAmf.getValue(2)).get(1);
			this.voiceFactory = new MsgVoiceFactory();
			this.videoFactory = new MsgVideoFactory();
			this.tsFactory = new HlsFactory(name);
			msgFactory.getRtmpSocket().pushBytes(objAmfT);
			break;
		case "createStream":
			ObjAmf objAmfT2 = Amf0Builder.mkResult((double)(objAmf.getValue(1)));
			objAmfT2.setType(objAmf.getType());
			msgFactory.getRtmpSocket().pushBytes(objAmfT2);
			break;
		case "publish":
			//ObjAmf objAmf3 = Amf0Builder.mkOnStatusPublish((double)(objAmf.getValue(1)));
			/*
			ObjAmf objAmf3 = Amf0Builder.mkOnStatusPublish((double)(0));
			objAmf3.setType(objAmf.getType());
			msgFactory.getRtmpSocket().pushBytes(objAmf3);*/
			BasicChunk bc4 = ChunkBuilder.makeUserControlMsg((byte) 0, ByteUtil.intToBytes(1));
			msgFactory.getRtmpSocket().pushBytes(bc4);
			ObjAmf objAmf4 = Amf0Builder.mkResult((double)(objAmf.getValue(1)));
			objAmf4.setType(objAmf.getType());
			msgFactory.getRtmpSocket().pushBytes(objAmf4);
			break;
		case "play":
			break;
		case "pause":
			break;
		case "onstatus":
			break;
		case "result":
			break;
		case "deleteStream":
			this.msgFactory.close();
			break;
		default:
			System.out.println("Experience Command Msg");
		}
	}
	
	public void close(){
		this.tsFactory.close();
	}
	
}
