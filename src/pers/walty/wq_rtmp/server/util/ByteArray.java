package pers.walty.wq_rtmp.server.util;

public class ByteArray {
	private byte[] data;
	private int index;
	private int length;
	public ByteArray(byte[] data){
		this.data = data;
		this.index = 0;
		this.length = data.length;
	}
	
	public void moveIndex(int len){
		this.index += len; 
	}
	
	public void moveIndex(){
		this.moveIndex(1);
	}
	
	public void moveIndex2End(){
		this.index = this.length - 1;
	}
	
	public byte[] getByteArray(int index,int len){
		int end = index+len > length ? length-1:index+len-1;
		return ByteUtil.subByteArray(this.data, index, end);
	}
	
	public int getSurplusLen(){
		return this.length - this.index;
	}
	
	public int getLength() {
		return length;
	}
	
	public boolean isFull(){
		return this.getSurplusLen() <= 1 ? true : false;
	}
	
	public byte[] getByteArray(int len){
		byte[] reBytes = null;
		if(!this.isFull()){
			if(len != 0){
				reBytes = this.getByteArray(this.index,len);
			}
			this.moveIndex(len);
		}
		return reBytes;
	}
}
