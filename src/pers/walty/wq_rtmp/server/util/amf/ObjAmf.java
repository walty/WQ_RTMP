package pers.walty.wq_rtmp.server.util.amf;

public interface ObjAmf {
	public byte[] makeBytes();
	public Object getValue(String key);
	public Object getValue(int index);
	public int getType();
	public void setType(int type);
}
