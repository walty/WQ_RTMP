package pers.walty.wq_rtmp.server.util.amf;

public enum Amf0 {
    kDouble((byte) 0),
    kBoolean((byte)1),
    //kBoolean = 0x01;
    kString((byte)2),
    kObject((byte)3),  
    //public static final byte kMovieClip = 0x04;
    //public static final byte kNull = 0x05;
    kNull((byte)5),
    //public static final byte kUndefined = 0x06;
    //public static final byte kReference = 0x07;
    //public static final byte kEcmaArray = 0x08;
    kEndOfObject((byte)9);
    //public static final byte kArray = 0x0A;
    //public static final byte kDate = 0x0B;
    //public static final byte kLongString = 0x0C;
    //public static final byte kUnsupported = 0x0D;
    //public static final byte kRecordset = 0x0E;
    //public static final byte kXml = 0x0F;
   // public static final byte kClassObject = 0x10;
    //public static final byte kInvalid = 0xFF;
    //public static final byte kNone = 0x100; 
    private byte b;
    private Amf0(byte b){
    	this.b = b;
    }
    public byte getB() {
		return b;
	}
	public void setB(byte b) {
		this.b = b;
	}
	
	public static Amf0 getAmf0(byte b){
		for(Amf0 a : Amf0.values()){
			if(b == a.getB()){
				return a;
			}
		}
		return null;
	} 
}
