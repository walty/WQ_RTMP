package pers.walty.wq_rtmp.server.util.amf;

import java.util.ArrayList;
import java.util.List;

public class Amf0Builder {
	public static ObjAmf0 mkResult(double affairsId){
		List list = new ArrayList();
		list.add("_result");
		list.add(affairsId);
		return new ObjAmf0(list);
	}
	
	public static ObjAmf0 mkOnStatus(double affairsId,String level,String code,String description){
		List list = new ArrayList();
		list.add("onStatus");
		list.add(affairsId);
		list.add(null);
		List listObj = new ArrayList();
		listObj.add("level");
		listObj.add(level);
		listObj.add("code");
		listObj.add(code);
		listObj.add("description");
		listObj.add(description);
		list.add(listObj);
		return new ObjAmf0(list);
	}
	
	public static ObjAmf0 mkOnStatusPublish(double affairsId){
		return mkOnStatus(affairsId,"status","Netstream.publish.Start","1");
	}
}
