package pers.walty.wq_rtmp.server.util.amf;

import java.util.List;

public class ObjAmf0 implements ObjAmf{
	private List list;
	private int type;
	public ObjAmf0(List list){
		this.list = list;
	}
	
	public void setType(int type){
		this.type = type;
	}
	
	public int getType(){
		return this.type;
	}
	
	public Object getValue(String key){
		int iKey = this.list.indexOf(key);
		return list.get(iKey+1);
	}
	public Object getValue(int index){
		return list.get(index);
	}
	
	public byte[] makeBytes(){
		return DealAmf0.mkBytes(this.list);
	}
	
	public String toString(){
		return this.list.toString();
	}
}
