package pers.walty.wq_rtmp.server.util.amf;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import pers.walty.wq_rtmp.server.message.MsgContent;
import pers.walty.wq_rtmp.server.util.ByteUtil;

public class DealAmf0 implements DealAmf{
	private List reList;
	
	public static ObjAmf0 getObjAmf0(List list){
		return new ObjAmf0(list);
	}
	
	public List deal(MsgContent msgContent){
		reList = new ArrayList();
		while(!msgContent.isFull()){
			switch(Amf0.getAmf0(msgContent.next())){
			case kDouble:
				reList.add(dealDouble(msgContent));
				break;
			case kString:
				reList.add(dealString(msgContent));
				break;
			case kObject:
				reList.add(dealObject(msgContent));
				break;
			case kNull:
				//msgContent.next();
				break;
			default:
				System.out.println("Experience dont know key");
				break;
			}
		}
		return reList;
	}
	
	public List dealObject(MsgContent msgContent){
		List reList = new ArrayList();
		byte[] b = msgContent.subContAndMvIndex(2);
		int bi = ByteUtil.bytesToInt(b);
		reList.add(new String(msgContent.subContAndMvIndex(bi)));
		while(Amf0.getAmf0(msgContent.getNext()) != Amf0.kEndOfObject){
			switch(Amf0.getAmf0(msgContent.next())){
			case kString:
				dealString4Obj(msgContent,reList);
				break;
			default:
				System.out.println("Experience type of Object");
			}
		}
		return reList;
	}
	
	public void dealString4Obj(MsgContent msgContent,List relist){
		for(int i = 0 ; i < 2 ; i++){
			byte[] b = msgContent.subContAndMvIndex(2);
			int bi = ByteUtil.bytesToInt(b);
			if(bi>0){
				relist.add(new String(msgContent.subContAndMvIndex(bi)));
			}
		}
	}
	
	public String dealString(MsgContent msgContent){
		byte[] b = msgContent.subContAndMvIndex(2);
		int bi = ByteUtil.bytesToInt(b);
		return new String(msgContent.subContAndMvIndex(bi));
	}
	public double dealDouble(MsgContent msgContent){
		byte[] b = msgContent.subContAndMvIndex(8);
		return ByteUtil.bytes2Double(b);
	}
	
	public static byte[] mkBytes(List list){
		Iterator i = list.iterator();
		byte[] reB = new byte[0];
		while(i.hasNext()){
			reB = ByteUtil.concatAll(reB, mkBytes(i.next()));
		}
		return reB;
	}
	
	private static byte[] mkBytes(Object obj){
		byte[] reB = null;
		if( obj instanceof Double){
			Double d = (Double) obj;
			byte[] b = ByteUtil.double2Bytes(d);
			reB = ByteUtil.concatAll(new byte[]{Amf0.kDouble.getB()}, b);
		}else if(obj instanceof List){
			List list = (List)obj;
			reB = mkBytes4Obj(list);
		}else if(obj instanceof String){
			byte[] type = {Amf0.kString.getB()};
			reB = ByteUtil.concatAll(type, str2Bytes(obj));
		}else if (null == obj){
			reB = new byte[]{Amf0.kNull.getB()};
		}else{
			System.out.println("Experience instanceof");
		}
		return reB;
	} 
	
	private static byte[] mkBytes4Obj(List list){
		byte[] reB = new byte[]{Amf0.kObject.getB()};
		Iterator iterator = list.iterator();
		if(iterator.hasNext()){
			reB = ByteUtil.concatAll(reB, str2Bytes(iterator.next()));
		}
		while(iterator.hasNext()){
			reB = ByteUtil.concatAll(reB, new byte[]{Amf0.kString.getB()});
			for(int i = 0 ; i < 2 ; i++){
				if(iterator.hasNext()){
					reB = ByteUtil.concatAll(reB, str2Bytes(iterator.next()));
				}else{
					reB = ByteUtil.concatAll(reB, new byte[]{0,0});
				}
			}
		}
		reB = ByteUtil.concatAll(reB, new byte[]{Amf0.kEndOfObject.getB()});
		return reB;
	}
	
	private static byte[] str2Bytes(Object obj){
		byte[] strB = ((String)obj).getBytes();
		byte[] len = ByteUtil.intToBytes(strB.length);
		byte[] len2 = {len[2],len[3]};
		return ByteUtil.concatAll(len2, strB);
	}
}
