package pers.walty.wq_rtmp.server.util;

import java.util.Arrays;
import java.util.List;

public class ByteUtil {
	
	//闭区间，下表
	public static byte[] subByteArray(byte[] val , int startIndex, int endIndex){
		byte[] reByte = new byte[endIndex - startIndex + 1];
		for(int i = startIndex,j = 0 ; i < endIndex+1 ; i++,j++){
			reByte[j] = val[i]; 
		}
		return reByte;
	}
	
	public static byte[] concatAll(byte[] first, byte[]... rest) {  
		  int totalLength = first.length;  
		  for (byte[] array : rest) {  
		    totalLength += array.length;  
		  }  
		  byte[] result = Arrays.copyOf(first, totalLength);  
		  int offset = first.length;  
		  for (byte[] array : rest) {  
		    System.arraycopy(array, 0, result, offset, array.length);  
		    offset += array.length;  
		  }  
		  return result;  
	}
	
	/**
	 * @param res the length must less than 4;
	 * */
	public static int bytesToInt(byte[] res){
		if(res == null){
			return 0;
		}
		byte[] inRes = {0,0,0,0};
		for(int i = 0 ; i < res.length ; i++){
			inRes[4 - res.length + i] = res[i];
		}
		int reInt;
		reInt = (inRes[3] & 0xff) | ((inRes[2] << 8) & 0xff00) | ((inRes[1]<< 24)>>>8) | (inRes[0]<<24); 
		return reInt;
	}
	
	public static byte[] intToBytes(int a){
		  return new byte[] {  
			        (byte) ((a >> 24) & 0xFF),  
			        (byte) ((a >> 16) & 0xFF),     
			        (byte) ((a >> 8) & 0xFF),     
			        (byte) (a & 0xFF)  
			    };  
	}
	
	public static long bytesToLong(byte[] res){
		return Integer.toUnsignedLong(bytesToInt(res));
	}
	
	/**
	 * 
	 * */
	public static byte[] subListToBArr(List<Byte> list,int start,int length){
		int len = list.size();
		if(start + len < length){
			return null;
		}
		byte[] reB = new byte[len];
		for(int i = start,j=0 ; i < start + len ; i++,j++){
			reB[j] = list.get(i);
		}
		return reB;
	}
	
	public static double bytes2Double(byte[] arr){
		long value = 0;  
        for (int i = 0, j = 7; i < 8; i++,j--) {  
            value |= ((long) (arr[j] & 0xff)) << (8 * i);  
        }  
        return Double.longBitsToDouble(value);  
	}
	
	public static byte[] double2Bytes(double d){ 
        long value = Double.doubleToRawLongBits(d);  
        byte[] byteRet = new byte[8];  
        for (int i = 0,j=7; i < 8; i++,j--) {  
            byteRet[j] = (byte) ((value >> 8 * i) & 0xff);  
        } 
        return byteRet;  
	}
	
	public static boolean bytesEquals(byte[] b1,byte[] b2){
		return Arrays.equals(b1, b2);
	}
	
	
	public static byte intToByte(int x) {  
	    return (byte) x;  
	} 
	
	public static byte[] fillBytes(int len,byte data){
		return fillBytes(len,new byte[]{data});
	}
	
	public static byte[] subBytes(int len,byte[] data){
		if(len > data.length){
			return null;
		}
		byte[] reB = new byte[len];
		for(int i = data.length - 1 ; i >= data.length - len ; i--){
			reB[i-1] = data[i];
		}
		return reB;
	}
	
	public static byte[] fillBytes(int len,byte[] data){
		if(len < data.length){
			return data;
		}
		byte[] reB = new byte[len];
		for(int i = 0 ; i < len - data.length ; i++){
			reB[i] = 0;
		}
		for(int i = 0 ; i < data.length ; i++){
			reB[i+len] = data[i];
		}
		return reB;
	}
	
	public static String bytes2Str(byte[] bs){
		StringBuffer sb = new StringBuffer();
		for (byte b : bs) {
			sb.append(b+";");
		}
		return sb.toString();
	}
	
	public static byte[] longToByteArray(long s){ 
		byte[] targets = new byte[8];
        for (int i = 0; i < 8; i++) {
            int offset = (targets.length - 1 - i) * 8;
            targets[i] = (byte) ((s >>> offset) & 0xff);
        }
	    return targets;
    } 
}
