package pers.walty.wq_rtmp.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.List;

public class RTMPServerSocket extends Thread{
	private List<RTMPSocket> linkList;
	private ServerSocket serverSocket;
	public RTMPServerSocket() throws IOException{
		super();
		serverSocket = new ServerSocket(3610);
		linkList = new ArrayList<RTMPSocket>();
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		super.run();
		while(true){
			RTMPSocket rtmpSocket;
			try {
				rtmpSocket = new RTMPSocket(serverSocket.accept());
				rtmpSocket.start();
				linkList.add(rtmpSocket);
				System.out.println("A new rtmp link in!");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
			}
		}
	}
	
	public List<RTMPSocket> getLinkList(){
		return this.linkList;
	}
	
	public int getLinkCount(){
		return this.linkList.size();
	}
	
}
