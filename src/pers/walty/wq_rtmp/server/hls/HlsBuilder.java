package pers.walty.wq_rtmp.server.hls;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import pers.walty.wq_rtmp.server.message.datamsg.AacFrame;
import pers.walty.wq_rtmp.server.message.datamsg.H264Frame;
import pers.walty.wq_rtmp.server.util.ByteArray;

public class HlsBuilder {
	public static final int MAX_TS_SIZE = 188;
	public static final int MAX_PES_DATA_SIZE = 65535;
	public static final byte TS_TYPE_NO_ADAPTION = 1;
	public static final byte TS_TYPE_ADAPTION = 3;
	public static final byte TS_PAT_PID = 0;
	public static final byte TS_PMT_PID = 66;
	public static final byte TS_AAC_PID = 68;
	public static final byte TS_H264_PID = 69;
	
	private M3u8File m3u8File;
	private byte[] h264First;
	private boolean isH264First;
	private byte aacCount;
	private byte h264Count;
	private PesList pesList;
	private ArrayList<TsBean> tsBeanList;
	
	public HlsBuilder(M3u8File m3u8File) {
		// TODO Auto-generated constructor stub
		this.m3u8File = m3u8File;
		pesList = new PesList();
		tsBeanList = new ArrayList<TsBean>();
		initAll();
	}
	
	public void close(){
		h264First = null;
		pesList = null;
		tsBeanList.clear();
		tsBeanList = null;
	}
	
	private void initAll(){
		isH264First = true;
		aacCount = 0;
		h264Count = 0;
		tsBeanList.clear();
		pesList.clear();
	}
	
	
	private boolean isH264First() {
		boolean re = isH264First;
		isH264First = false;
		return re;
	}
	
	public void pushAacFrame(AacFrame aacFrame){
		try {
			pesList.add(PesBuilder.mkAacPes(aacFrame));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void pushH264Frame(H264Frame h264Frame){
		if(!pesList.isEmpty() && h264Frame.getH264FrameType() == H264Frame.I_FRAME){
			pesList.insertPtsDts();
			paketPesToTsBeanList();
			try {
				saveFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			initAll();
		}
		byte[] h264First = isH264First()?this.h264First:null;
		Pes pes = null;
		try {
			pes = PesBuilder.mkH264Pes(h264Frame, h264First);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		pesList.add(pes);
	}
	
	public void saveFile() throws IOException{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		baos.write(Pis.TS_PAT);
		baos.write(Pis.TS_PMT);
		Iterator<TsBean> iterator = tsBeanList.iterator();
		while(iterator.hasNext()){
			baos.write(iterator.next().flushAllData());
		}
		m3u8File.save(pesList.getDeTime(), baos.toByteArray());
	}
	
	public void paketPesToTsBeanList(){
		Iterator<Pes> iterator = pesList.iterator();
		while(iterator.hasNext()){
			try {
				tsBeanList.add(mkTsBean(iterator.next()));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void setH264First(byte[] h264First) {
		this.h264First = h264First;
	}
	
	private TsBean mkTsBean(Pes pes) throws IOException{
		ByteArray byteArray = new ByteArray(PesBuilder.pesToData(pes));
		TsBean tsBean = new TsBean();
		byte start = 1;
		while(!byteArray.isFull()){
			int len = (pes.getStreamType()==Pes.TS_H264_TYPE&&start==1) ? MAX_TS_SIZE - 12 : byteArray.getSurplusLen() >= MAX_TS_SIZE - 4 ? MAX_TS_SIZE - 4 : byteArray.getSurplusLen();
			byte adaptation = (pes.getStreamType()==Pes.TS_H264_TYPE&&start==1) ? TS_TYPE_ADAPTION : (len == MAX_TS_SIZE - 4) ? TS_TYPE_NO_ADAPTION : TS_TYPE_ADAPTION;
			byte[] data2 = byteArray.getByteArray(len);
			byte pid = pes.getStreamType() == Pes.TS_AAC_TYPE ? TS_AAC_PID : TS_H264_PID;
			tsBean.add(TsBuilder.mkTs(pes,data2,start,pid,adaptation,pesCountUp(pes)));
			start = 0; 
		}
		//pesCountUp(pes);
		return tsBean;
	}
	
	private byte pesCountUp(Pes pes){
		byte c = 0;
		if(pes.getStreamType() == Pes.TS_AAC_TYPE){
			c = (byte) ((aacCount++)%16);
		}else if(pes.getStreamType() == Pes.TS_H264_TYPE){
			c = (byte) ((h264Count++)%16);
		}
		return c;
	}
	
}
