package pers.walty.wq_rtmp.server.hls;

import pers.walty.wq_rtmp.server.message.datamsg.Frame;
import pers.walty.wq_rtmp.server.message.datamsg.H264Frame;

public class Pes{
	public static final byte TS_AAC_TYPE = 0x0f;
	public static final byte TS_H264_TYPE = 0x1b;
	private byte[] pesHeader;
	private byte[] pts;
	private byte[] dts;
	private Frame frame;
	private long pcrTime;
	
	private byte streamType;
	public Pes(byte streamType,byte[] pesHeader,Frame frame){
		setStreamType(streamType);
		setPesHeader(pesHeader);
		setFrame(frame);
	}
	
	public void setPcrTime(long pcrTime) {
		this.pcrTime = pcrTime;
	}
	
	public long getPcrTime() {
		return pcrTime;
	}
	
	public void setPts(long ptsL) {
		this.pts = PesBuilder.long2ByteArray4DtsPts(streamType, ptsL);
	}
	
	public byte[] getPts() {
		return pts;
	}
	
	public void setDts(long dtsL) {
		this.pcrTime = dtsL;
		this.dts = PesBuilder.long2ByteArray4DtsPts((byte) 0, dtsL);
	}
	
	public byte[] getDts() {
		return dts;
	}
	
	public int getFrameDtsTime(){
		return frame.getDtsTime();
	}
	
	public byte[] getFrameData(){
		return frame.getData();
	}
	
	public int getFrameCompositionTime(){
		return ((H264Frame)frame).getCompositionTime();
	}
	
	public void setFrame(Frame frame) {
		this.frame = frame;
	}
	public Frame getFrame() {
		return frame;
	}
	public void setPesHeader(byte[] pesHeader) {
		this.pesHeader = pesHeader;
	}
	public byte[] getPesHeader() {
		return pesHeader;
	}
	
	public void setStreamType(byte streamType) {
		this.streamType = streamType;
	}
	
	public byte getStreamType() {
		return streamType;
	}
}
