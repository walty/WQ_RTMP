package pers.walty.wq_rtmp.server.hls;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class TsBean {
	public static final byte TS_AAC = 0x0f;
	public static final byte TS_H264 = 0x1b;
	private List<Ts> tsList;
	private byte streamType;
	private int time;
	
	public TsBean(List<Ts> tsList){
		setTsList(tsList);
		setTime(0);
	}
	
	public TsBean(){
		setTsList(new LinkedList<Ts>());
		setTime(0);
	}
	
	public byte[] flushAllData() throws IOException{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		Iterator<Ts> i = this.iterator();
		while(i.hasNext()){
			baos.write(i.next().getData());
		}
		return baos.toByteArray();
	}
	
	public void add(Ts ts){
		tsList.add(ts);
	}
	
	public byte[] getPid(){
		return tsList.get(0).getPid();
	}
	
	public Iterator<Ts> iterator(){
		return tsList.iterator();
	}
	
	public byte getStreamType() {
		return streamType;
	}
	
	public void setStreamType(byte streamType) {
		this.streamType = streamType;
	}
	
	public void setTsList(List<Ts> tsList) {
		this.tsList = tsList;
	}
	
	public List<Ts> getTsList() {
		return tsList;
	}
	
	public void setTime(int time) {
		this.time = time;
	}
	
	public int getTime() {
		return time;
	}
}
