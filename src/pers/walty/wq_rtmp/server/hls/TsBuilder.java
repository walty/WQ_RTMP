package pers.walty.wq_rtmp.server.hls;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import pers.walty.wq_rtmp.server.util.ByteUtil;

public class TsBuilder {
	public static Ts mkTs(Pes pes,byte[] data,byte start,byte pid,byte adaptation_field_control,byte continuity_counter) throws IOException{
		Ts reTs = null;
		int len = data.length;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		baos.write(0x47);					//sync_byte
		byte b = (byte) (start == 1 ? 0x40:0);//00000001
		baos.write(b);						//transport_error_indicator,payload_unit_start_indicator,transport_priority,pid
		baos.write(pid);					//pid
		b = (byte) 0x3f;
		byte adaptation = adaptation_field_control;
		adaptation_field_control = (byte) (adaptation_field_control<< 4);
		adaptation_field_control |= 0xcf;
		b &= adaptation_field_control;
		continuity_counter |= 0xf0;
		b &= continuity_counter;
		baos.write(b); 						//transport_scrambling_control,adaptation_field_control,continuity_counter
		if(adaptation == HlsBuilder.TS_TYPE_ADAPTION && HlsBuilder.MAX_TS_SIZE - 4 - len > 0){
			long pcr = start == 1 && pid== HlsBuilder.TS_H264_PID ? pes.getPcrTime():0;
			byte[] adap = mkAdaption(HlsBuilder.MAX_TS_SIZE - 4 - len,pcr);
			if(null != adap)
				baos.write(adap);
		}
		baos.write(data);
		reTs = new Ts(baos.toByteArray());
		return reTs;
	}
	//xxtt821
	private static byte[] mkAdaption(int len,long pcr){
		byte[] reBytes = null;
		if(len == 1){
			reBytes = new byte[]{0x00};
		}else if(len == 2){
			reBytes = new byte[]{0x01,0x00};
		}else if(len > 2){
			reBytes = new byte[len];
			reBytes[0] = (byte) (len - 1);
			reBytes[1] = (byte) (pcr==0?0:0x10);
			int i = 2;
			if(pcr!=0){
				pcr = pcr<<15;
				byte[] b = ByteUtil.longToByteArray(pcr);
				for(;i<8;i++){
					reBytes[i] = b[i];
				}
			}
			for(; i < len ; i++){
				reBytes[i] =  (byte) 0xff;
			}
		}
		return reBytes;
	}
}
