package pers.walty.wq_rtmp.server.hls;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import pers.walty.wq_rtmp.server.message.datamsg.AacFrame;
import pers.walty.wq_rtmp.server.message.datamsg.Frame;
import pers.walty.wq_rtmp.server.message.datamsg.H264Frame;
import pers.walty.wq_rtmp.server.util.ByteUtil;

public class PesBuilder {
	public static final byte[] PES_H264_HEADER = {
		0x00,0x00,0x01,			//pesStartCode
		(byte)0xe0,				//streamId
		0x00,0x00,				//pesPacketLength
		(byte)0x80,				//flag1
		(byte)0xc0,				//flag2
		(byte)0x0a};			//pesDataLength
	
	public final static byte[] PES_AAC_HEADER(int intLen){
		byte[] len = ByteUtil.intToBytes(intLen + 8);
		byte[] reByte = {
				0x00,0x00,0x01,	//pesStartCode
				(byte)0xc0,		//streamId
				len[2],len[3],	//pesPacketLength
				(byte)0x80,		//flag1
				(byte)0x80,		//flag2
				(byte)0x05,		//pesDataLength
		};
		
		return reByte;
	} 
	
	private static Pes _mkH264Pes(Frame frame,byte[] h264First) throws IOException{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		baos.write(new byte[]{0,0,0,1,9,(byte) 0xf0});
		if(null != h264First){
			baos.write(h264First);
		}
		baos.write(frame.getData());
		frame.setData(baos.toByteArray());
		return new Pes(Pes.TS_H264_TYPE,PES_H264_HEADER,frame);
	}
	
	private static Pes _mkAacPes(Frame frame) throws IOException{
		byte[] data = frame.getData();
		return new Pes(Pes.TS_AAC_TYPE,PES_AAC_HEADER(data.length),frame);
	}
	
	public static Pes mkAacPes(AacFrame aacFrame) throws IOException{
		return _mkAacPes(aacFrame);
	}
	
	public static Pes mkH264Pes(H264Frame h264Frame,byte[] h264First) throws IOException{
		return _mkH264Pes(h264Frame, h264First);
	}
	
	public static byte[] pesToData(Pes pes) throws IOException{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		baos.write(pes.getPesHeader());
		if(pes.getStreamType() == Pes.TS_AAC_TYPE){
			baos.write(pes.getPts());
		}else if(pes.getStreamType() == Pes.TS_H264_TYPE){
			baos.write(pes.getPts());
			baos.write(pes.getDts());
		}
		baos.write(pes.getFrameData());
		return baos.toByteArray();
	}
	
	public static byte[] long2ByteArray4DtsPts(byte flag,long v){
		byte b = (byte) (flag==0?1:flag==Pes.TS_AAC_TYPE?2:3);
		byte[] d = ByteUtil.longToByteArray(v);
		byte[] re = new byte[5];
		//d[7]1111111 1
		re[4] = (byte) (d[7]<<1 | 0x01);
		//d[6]1111111 d[7]1
		re[3] = (byte) ((d[7] & 0x80)>>>7 | d[6]<<1);
		//d[5]111111 d[6]1 1
		re[2] = (byte) (((d[6] & 0x80) >>> 6) | (d[5] << 2) | 0x01);
		//d[4]111111 d[5]11
		re[1] = (byte) (((d[5] & 0xc0)>>>6) | d[4]<<2);
		//00 b11  d[4]11 1
		re[0] = (byte) ((d[4]&0x01)<<3 | 0x01 | b<<4);
		return re;
	}
	
}
