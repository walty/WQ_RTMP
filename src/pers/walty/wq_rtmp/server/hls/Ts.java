package pers.walty.wq_rtmp.server.hls;

import pers.walty.wq_rtmp.server.util.ByteUtil;

public class Ts {
	
	private byte[] data;
	
	public Ts(byte[] data){
		this.data = data;
	}
	
	public byte[] getData() {
		return data;
	}
	
	public byte[] getPid(){
		byte[] reBs = ByteUtil.subByteArray(data, 1, 2);
		reBs[0] |= 0xe0;
		return reBs;
	}
	
}
