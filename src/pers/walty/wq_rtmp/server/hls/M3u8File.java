package pers.walty.wq_rtmp.server.hls;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.LinkedList;

import pers.walty.wq_rtmp.server.message.datamsg.SaveWorker;

public class M3u8File {
	public static final String SAVE_DATA_PATH = "./videoTemp";
	public static final String M3U8_FILE_NAME = "walty.m3u8";
	public static final String TS_SUFFIX = ".ts";
	private String flodeName;
	private int count;
	private File file;
	private FileWriter fWriter;
	private DecimalFormat df;   
	private LinkedList<String> strList;
	public M3u8File(String flodeName){
		count = 0;
		this.flodeName = flodeName;
		df  = new DecimalFormat("######0.000"); 
		strList = new LinkedList<String>();
		try {
			newM3u8File();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void save(float deTime,byte[] data) throws IOException{
		saveM3u8File(deTime/100000);
		deletTsFile();
		saveTsFile(data);
	}
	
	private void newM3u8File() throws IOException{
		file = new File(SAVE_DATA_PATH + "/" + flodeName + "/" + M3U8_FILE_NAME);
		SaveWorker.checkFile(file);
		this.fWriter = new FileWriter(file,false);
	}
	
	public void saveM3u8File(float deTime) throws IOException{
		newM3u8File();
		strList.add("\n#EXTINF:"+df.format(deTime)+",\n./" + count + TS_SUFFIX);
		if(strList.size() > 3){
			strList.removeFirst();
		}
		StringBuffer sb = new StringBuffer();
		Iterator<String> iterator = strList.iterator();
		while(iterator.hasNext()){
			sb.append(iterator.next());
		}
		
		fWriter.write("#EXTM3U\n"+
				"#EXT-X-VERSION:3\n"+
				"#EXT-X-ALLOW-CACHE:NO\n"+
				"#EXT-X-TARGETDURATION:4\n"+
				"#EXT-X-MEDIA-SEQUENCE:"+(count+1)+"\n" + sb.toString());
		fWriter.flush();
		fWriter.close();
	}
	
	public void saveTsFile(byte[] data) throws IOException{
		File file = new File(SAVE_DATA_PATH + "/" + flodeName + "/" + (count++) + TS_SUFFIX);
		SaveWorker.checkFile(file);
		FileOutputStream fos = new FileOutputStream(file);
		fos.write(data);
		fos.close();
	}
	
	public void deletTsFile(){
		int deCont = count - 6;
		if(deCont >= 0){
			File file = new File(SAVE_DATA_PATH + "/" + flodeName + "/" + deCont  + TS_SUFFIX );
			file.delete();
		}
	}
	
	public void close(){
		File parent = file.getParentFile();
		boolean a = file.delete();
		File[] containFiles = parent.listFiles();
		if(null != containFiles){
			for (File f : containFiles) {
				f.delete();
			}
		}
		boolean b = parent.delete();
	}
	
}
