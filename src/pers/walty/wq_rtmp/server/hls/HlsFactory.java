package pers.walty.wq_rtmp.server.hls;

import pers.walty.wq_rtmp.server.message.datamsg.AacFrame;
import pers.walty.wq_rtmp.server.message.datamsg.H264Frame;

public class HlsFactory {
	private HlsBuilder hlsBuilder;
	private String foldeName;
	private M3u8File m3u8File;
	
	public HlsFactory(String foldeName){
		m3u8File = new M3u8File(foldeName);
		hlsBuilder = new HlsBuilder(m3u8File);
		setFoldeName(foldeName);
	}
	
	public void pushH264Frame(H264Frame h264Frame){
		hlsBuilder.pushH264Frame(h264Frame);
	}
	
	public void pushAacFrame(AacFrame aacframe){
		hlsBuilder.pushAacFrame(aacframe);
	}
	
	public void setAvcSequenceHeader(byte[] data){
		hlsBuilder.setH264First(data);
	}
	
	public void setFoldeName(String foldeName) {
		this.foldeName = foldeName;
	}
	
	public String getFoldeName() {
		return foldeName;
	}
	
	public void close(){
		hlsBuilder.close();
		m3u8File.close();
	}
	
}
