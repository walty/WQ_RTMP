package pers.walty.wq_rtmp.server.hls;

import java.util.ArrayList;
import java.util.Iterator;

public class PesList{
	public static final int AUDIO_SAMPLES_PER_FRAME = 1024;
	public static final int TIME_SCALE = 90000;
	public static final int INITIAL_VALUE = 1;
	public static final long PTS_DTS_MAX = 8589934592L;
	
	private ArrayList<Pes> pesList;
	private int videoFps = 20;
	private int audioSampleRate = 48000;
	private int audioOffset;
	private int videoOffset;
	private long videoDts;
	private long audioPts;
	private long now_time;
	private long old_time;
	
	public PesList() {
		// TODO Auto-generated constructor stub
		pesList = new ArrayList<Pes>();
		videoDts = INITIAL_VALUE;
		audioPts = INITIAL_VALUE;
		audioOffset = TIME_SCALE*AUDIO_SAMPLES_PER_FRAME/audioSampleRate;
		videoOffset = TIME_SCALE/videoFps;
		now_time = 0;
		old_time = 0;
	}
	
	public void add(Pes e){
		pesList.add(e);
	}
	
	public void clear(){
		pesList.clear();
		old_time = now_time;
	}
	
	public void close(){
		pesList.clear();
		pesList = null;
	}
	
	public void insertPtsDts(){
		Iterator<Pes> iterator = pesList.iterator();
		while(iterator.hasNext()){
			insertPtsDts4Pes(iterator.next());
		}
	}
	
	public boolean isEmpty(){
		return pesList.isEmpty();
	}
	
	public long getAudioPts() {
		audioPts = (audioPts+audioOffset)%PTS_DTS_MAX;
		return audioPts;
	}
	
	public long getVideoDts() {
		videoDts = (videoDts+videoOffset)%PTS_DTS_MAX;
		return videoDts;
	}
	
	private void insertPtsDts4Pes(Pes pes){
		now_time =	pes.getFrameDtsTime()*10;
		if(pes.getStreamType() == Pes.TS_AAC_TYPE){
			//pes.setPts(this.getAudioPts());
			pes.setPts(now_time);
		}else{
			pes.setPts(now_time+pes.getFrameCompositionTime()*10);
			pes.setDts(now_time);
		}
	}
	
	public float getDeTime(){
		return now_time - old_time;
	}
	
	public Iterator<Pes> iterator(){
		return pesList.iterator();
	}
	
}
